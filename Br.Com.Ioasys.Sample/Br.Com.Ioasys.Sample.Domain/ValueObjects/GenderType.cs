﻿namespace Br.Com.Ioasys.Sample.Domain.ValueObjects
{
    public enum GenderType
    {
        Male = 'M',
        Female = 'F'
    }
}
