﻿namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public enum StatusType
    {
        PreRegistration = 1,
        Active, 
        Inactive,
    }
}
