﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.ValueObjects
{
    public enum  VoteNoteType
    {
        Note0 = 0,
        Note1 = 1,
        Note2 = 2,
        Note3 = 3,
        Note4 = 4
    }
}
