﻿namespace Br.Com.Ioasys.Sample.Domain.ValueObjects
{
    public enum ErrorType
    {
        BadRequest = 400,
        InternalServerError = 500
    }
}
