﻿using System;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public abstract class Entity
    {
        public Guid Id { get; set; }
    }
}
