﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class MovieDetail : Entity
    {
        public Guid MovieId { get; set; }
        public Guid CountryId { get; set; }
        public string Language { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string AlsoKnownAs { get; set; }
        public virtual Country Country { get; set; }

        public virtual Movie Movie { get; set; }
    }
}
