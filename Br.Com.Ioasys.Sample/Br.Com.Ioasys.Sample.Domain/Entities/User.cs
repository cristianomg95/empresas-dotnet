﻿using System;
using System.Collections.Generic;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Bio { get; set; }
        public string Password { get; set; }
        public Guid PermissionId { get; set; }
        public StatusType Status { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual UserDetail UserDetail{ get; set; }
        public virtual UserEmail Email{ get; set; }
        public virtual IEnumerable<Vote> Votes { get; set; }

        public void GenerateUserId(string email)
        {
            int randomId;
            var random = new Random();
            var bytes = new Byte[100];
            random.NextBytes(bytes);
            randomId = bytes[0];

            var userId = email.Split('@')[0];

            NickName = userId.Insert(userId.Length, randomId.ToString());
        }
    }
}
