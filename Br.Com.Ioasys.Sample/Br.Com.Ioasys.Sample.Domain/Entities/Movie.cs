﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Movie : Entity
    {
        public Movie()
        {
            Votes = new List<Vote>();
        }
        public string Title { get; set; }
        public string Description{ get; set; }
        public string Genre { get; set; }
        public Guid DirectorId { get; set; }
        public virtual Director Director { get; set; }
        public virtual MovieDetail MovieDetail { get; set; }
        public virtual IEnumerable<Vote> Votes { get; set; }
        public virtual IEnumerable<MovieWrite> Writes { get; set; }
        public virtual IEnumerable<MovieActor> Actores { get; set; }


        public decimal CalculeAverageVote()
        {
            if (Votes.Count() > 0)
            {
                var sumVotes = Votes.Sum(x => (int)x.Note);

                return (sumVotes / Votes.Count());
            }
            return 0.0M;
        }
    }
}
