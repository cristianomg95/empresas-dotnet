﻿using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using System;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Vote : Entity
    {
        public Guid MovieId { get; set; }
        public Guid UserId { get; set; }
        public VoteNoteType Note  { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual User User { get; set; }
    }
}
