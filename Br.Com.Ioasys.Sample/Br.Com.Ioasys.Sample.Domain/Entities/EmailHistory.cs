﻿namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class EmailHistory : Entity
    {
        public string EmailTo { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public bool Sended {get; set; }
    }
}
