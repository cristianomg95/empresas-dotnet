﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Actor : Person
    {
        public virtual IEnumerable<MovieActor> Movies { get; set; }
    }
}
