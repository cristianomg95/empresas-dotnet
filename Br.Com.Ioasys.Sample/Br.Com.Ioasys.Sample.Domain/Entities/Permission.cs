﻿using System.Collections.Generic;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Permission : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual IEnumerable<User> Users { get; set; }
    }
}