﻿using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using System;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class UserDetail : Entity
    {
        public Guid UserId { get; set; }
        public GenderType Gender { get; set; }
        public DateTime DateOfBirthday { get; set; }
        public Guid CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual User User { get; set; }
    }
}
