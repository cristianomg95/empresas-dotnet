﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Person : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
