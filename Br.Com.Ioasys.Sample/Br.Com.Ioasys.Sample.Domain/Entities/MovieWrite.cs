﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class MovieWrite
    {
        public Guid MovieId { get; set; }
        public Guid WriterId { get; set; }

        public virtual Movie Movie{ get; set; }
        public virtual Writer Writer { get; set; }
}
}
