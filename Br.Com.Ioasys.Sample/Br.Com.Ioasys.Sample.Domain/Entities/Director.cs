﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Director : Person
    {
        public virtual IEnumerable<Movie> Movies { get; set; }

    }
}
