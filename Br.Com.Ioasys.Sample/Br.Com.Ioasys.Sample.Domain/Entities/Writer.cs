﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Writer : Person
    {
        public virtual IEnumerable<MovieWrite> Movies { get; set; }

    }
}
