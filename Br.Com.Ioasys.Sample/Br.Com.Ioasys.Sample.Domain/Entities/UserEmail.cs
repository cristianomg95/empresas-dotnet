﻿using System;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class UserEmail : Entity
    {
        public Guid UserId { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool Confirmed { get; set; }
        public virtual User User { get; set; }
    }
}
