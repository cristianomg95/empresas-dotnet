﻿using System.Collections.Generic;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class Country : Entity
    {
        public string Name { get; set; }
        public virtual IEnumerable<UserDetail> UserDetails { get; set; }
    }
}
