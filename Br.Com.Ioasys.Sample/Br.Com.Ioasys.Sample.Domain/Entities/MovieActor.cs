﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Entities
{
    public class MovieActor
    {
        public Guid MovieId { get; set; }
        public Guid ActorId { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual Actor Actor { get; set; }
    }
}
