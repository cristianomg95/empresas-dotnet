﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Configurations
{
    public class Credentials
    {
        public string JWTSecretKey { get; set; }
        public string AdministrationSecretKey { get; set; }
    }
}
