﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoResultMovieDetail : DtoMovieDetail
    {
        public decimal Median{ get; set; }
    }
}
