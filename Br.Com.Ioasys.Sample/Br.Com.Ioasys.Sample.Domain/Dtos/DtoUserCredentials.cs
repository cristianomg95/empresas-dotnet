﻿namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoUserCredentials
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
