﻿using System;
using System.Collections.Generic;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoCreateMovie 
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public Guid DirectorId { get; set; }
        public DtoMovieDetail MovieDetail { get; set; }
        public virtual IEnumerable<Guid> Writes { get; set; }
        public virtual IEnumerable<Guid> Actores { get; set; }
    }

}
