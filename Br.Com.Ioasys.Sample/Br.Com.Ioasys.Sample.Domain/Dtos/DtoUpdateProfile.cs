﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoUpdateProfile
    {
        public string NickName { get; set; }
        public string Bio { get; set; }
    }
}
