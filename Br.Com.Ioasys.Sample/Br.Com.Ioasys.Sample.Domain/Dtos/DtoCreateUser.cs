﻿namespace Br.Com.Ioasys.Business.Dtos
{
    public class DtoCreateUser
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
