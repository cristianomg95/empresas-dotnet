﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoMovieDetail
    {
        public Guid CountryId { get; set; }
        public string Language { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string AlsoKnownAs { get; set; }
    }
}
