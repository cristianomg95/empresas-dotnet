﻿using Br.Com.Ioasys.Business.Dtos;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoCreateAdministrationUser : DtoCreateUser
    {
        public string SecretKey { get; set; }
    }
}
