﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoEmailConfirm
    {
        public string Email { get; set; }
        public string Code { get; set; }

    }
}
