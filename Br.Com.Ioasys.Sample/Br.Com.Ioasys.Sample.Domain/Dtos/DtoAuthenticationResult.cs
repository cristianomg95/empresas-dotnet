﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoAuthenticationResult
    {
        public User User { get; set; }
        public string Token { get; set; }
        public string Permission { get; set; }
    }
}
