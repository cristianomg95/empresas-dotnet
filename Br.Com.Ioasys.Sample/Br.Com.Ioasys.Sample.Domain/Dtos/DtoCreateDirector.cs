﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Dtos
{
    public class DtoCreateDirector
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
