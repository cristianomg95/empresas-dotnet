﻿using System;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork
{
    public interface IUnitOfWork
    {
        int Save();
    }

}
