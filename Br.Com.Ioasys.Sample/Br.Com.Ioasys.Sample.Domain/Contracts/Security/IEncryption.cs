﻿namespace Br.Com.Ioasys.Sample.Domain.Contracts.Security
{
    public interface IEncryption
    {
        string GenerateCryptgraphy(string password);
        bool CheckPassword(string password, string encryptedPassword);
    }
}
