﻿using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Services
{
    public interface IAuthenticationService
    {
        DtoAuthenticationResult Execute(DtoUserCredentials dto);
    }
}
