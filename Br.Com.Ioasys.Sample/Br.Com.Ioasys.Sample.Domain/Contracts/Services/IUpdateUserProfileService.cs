﻿using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Services
{
    public interface IUpdateUserProfileService
    {
        User Execute(DtoUpdateProfile dto, string email);
    }
}
