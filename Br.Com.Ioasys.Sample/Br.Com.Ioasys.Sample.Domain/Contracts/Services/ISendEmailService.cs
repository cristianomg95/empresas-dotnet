﻿namespace Br.Com.Ioasys.Sample.Domain.Contracts.Services
{
    public interface ISendEmailService
    {
        bool Execute(string email, string content, string subject);
    }
}
