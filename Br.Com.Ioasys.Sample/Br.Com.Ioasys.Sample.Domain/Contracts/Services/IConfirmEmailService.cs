﻿using Br.Com.Ioasys.Sample.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Services
{
    public interface IConfirmEmailService
    {
        void Execute(DtoEmailConfirm dto);
    }
}
