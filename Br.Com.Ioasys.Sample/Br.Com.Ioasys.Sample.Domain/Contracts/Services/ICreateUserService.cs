﻿using Br.Com.Ioasys.Business.Dtos;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using System;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Services
{
    public interface ICreateUserService
    {
        User Execute(DtoCreateUser user, PermissionType permission);
    }
}
