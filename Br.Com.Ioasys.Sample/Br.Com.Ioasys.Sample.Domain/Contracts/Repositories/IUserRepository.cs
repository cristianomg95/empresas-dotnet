﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.LojaQueExplode.Infra.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetByEmail(string email);
    }
}
