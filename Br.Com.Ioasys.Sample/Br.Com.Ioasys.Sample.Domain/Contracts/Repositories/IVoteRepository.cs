﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.LojaQueExplode.Infra.Repositories.Abstract;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Repositories
{
    public interface  IVoteRepository : IBaseRepository<Vote>
    {
    }
}
