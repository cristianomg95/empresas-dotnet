﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.LojaQueExplode.Infra.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Br.Com.Ioasys.Sample.Domain.Contracts.Repositories
{
    public interface IPermissionRepository : IBaseRepository<Permission>
    {
        Permission GetByName(string name);
    }
}
