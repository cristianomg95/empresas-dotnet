﻿using Br.Com.Ioasys.Business.Exceptions;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Br.Com.Ioasys.Sample.Business.Services
{
    public class UpdateUserProfileSerivce :IUpdateUserProfileService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _uow;
        public UpdateUserProfileSerivce(IUserRepository userRepository,
            IUnitOfWork uow)
        {
            _userRepository = userRepository;
            _uow = uow;
        }
        public User Execute(DtoUpdateProfile dto, string email)
        {

            var user = _userRepository.GetByEmail(email);

            if (!string.IsNullOrEmpty(dto.Bio))
                user.Bio = dto.Bio;
            if (!string.IsNullOrEmpty(dto.NickName))
            {
                if (_userRepository.GetAll().FirstOrDefault(x => x.NickName == dto.NickName) == null)
                    user.NickName = dto.NickName;
            }

            if (_uow.Save() > 0)
                return user;

            return default;
        } 
    }
}
