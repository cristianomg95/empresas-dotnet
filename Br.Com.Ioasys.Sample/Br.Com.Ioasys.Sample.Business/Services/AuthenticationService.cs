﻿using Br.Com.Ioasys.Sample.Domain.Configurations;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Security;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Br.Com.Ioasys.Sample.Business.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncryption _encryption;
        private readonly Credentials _credentials;
        private readonly IPermissionRepository _permissionRepository;
        public AuthenticationService(IUserRepository userRepository,
            IEncryption encryption,
            Credentials credentials,
            IPermissionRepository permissionRepository)
        {
            _userRepository = userRepository;
            _encryption = encryption;
            _credentials = credentials;
            _permissionRepository = permissionRepository;
        }
        public DtoAuthenticationResult Execute(DtoUserCredentials dto)
        {
            var existingUser = _userRepository.GetByEmail(dto.Email);

            if (existingUser != null && existingUser.Status == StatusType.Active)
            {
                var correctPassword = _encryption.CheckPassword(dto.Password, existingUser.Password);

                if (correctPassword)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(_credentials.JWTSecretKey);

                    var permission = _permissionRepository.GetById(existingUser.PermissionId);

                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {

                            new Claim(ClaimTypes.Email, existingUser.Email.Description),
                            new Claim(ClaimTypes.Role, permission.Name),

                        }),
                        Expires = DateTime.UtcNow.AddHours(3),

                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };

                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    return new DtoAuthenticationResult { User = existingUser, Token = tokenHandler.WriteToken(token), Permission = permission.Name};
                }
            }
            return default;
        }
    }
}
