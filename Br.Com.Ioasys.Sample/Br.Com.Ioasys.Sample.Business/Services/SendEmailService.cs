﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Entities;

namespace Br.Com.Ioasys.Business.Sample.Services
{
    public class SendEmailService : ISendEmailService
    {
        private readonly IEmailHistoryRepository _emailHistoryRepository;
        private readonly IUnitOfWork _uow;
        public SendEmailService(IEmailHistoryRepository emailHistoryRepository,
            IUnitOfWork uow)
        {
            _emailHistoryRepository = emailHistoryRepository;
            _uow = uow;
        }
        public bool Execute(string email, string content, string subject)
        {
            _emailHistoryRepository.Insert(new EmailHistory
            {
                EmailTo = email,
                Content = content,
                Subject = subject,
                Sended = true
            });

            _uow.Save();

            return true;
        }
    }
}
