﻿using Br.Com.Ioasys.Business.Dtos;
using Br.Com.Ioasys.Business.Exceptions;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Security;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Br.Com.Ioasys.Business.Sample.Services
{
    public class CreateUserService : ICreateUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IUserRepository _userRepository;
        private readonly IEncryption _encryption;
        private readonly ISendEmailService _sendEmailService;
        private readonly IPermissionRepository _permissionRepository;

        public CreateUserService(IUnitOfWork uow,
            IUserRepository userRepository,
            IEncryption encryption,
            ISendEmailService sendEmailService,
            IPermissionRepository permissionRepository)
        {
            _uow = uow;
            _userRepository = userRepository;
            _encryption = encryption;
            _sendEmailService = sendEmailService;
            _permissionRepository = permissionRepository;
        }
        public User Execute(DtoCreateUser user, PermissionType permission)
        {
            ValidateUser(user);

            using (var scope = new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var _permission = _permissionRepository.GetByName(permission.ToString("G"));
                    var code = GenerateCode();
                    var newUser = new User
                    {
                        Name = user.Name,
                        Email = new UserEmail { Description = user.Email, Code = code },
                        Password = _encryption.GenerateCryptgraphy(user.Password),
                        Status = StatusType.PreRegistration,
                        PermissionId = _permission.Id,
                    };

                    newUser.GenerateUserId(user.Email);

                    _userRepository.Insert(newUser);
                    _uow.Save();

                    SendEmail(user.Email, code);
                    scope.Complete();

                    return newUser;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw new ServiceException(ex.Message);
                }
            }


        }
        private bool SendEmail(string email, string code)
        {
            var subject = "Confirmação de email";

            var content = $"To verify your email address," +
                $" please use the following One Time Password - {code} " +
                $"Do not share this OTP with anyone." +
                $" IMDb takes your account security very seriously." +
                $" IMDb Customer Service will never ask you to disclose or verify your IMDb password," +
                $" OTP, credit card, or banking account number." +
                $" If you receive a suspicious email with a link to update your account information," +
                $" do not click on the link—instead, report the email to IMDb for investigation." +
                $"Thank you for being a member of the IMDb community! We hope to see you again soon.";

            return _sendEmailService.Execute(email, content, subject);

        }
        private string GenerateCode()
        {
            var result = new List<string>();
            var random = new Random();

            while (result.Count < 6)
            {
                result.Add(random.Next(0, 9).ToString());
            }

            return string.Join(string.Empty, result);

        }
        private void ValidateUser(DtoCreateUser user)
        {
            var checkIfPasswordIsEquals = user.Password == user.ConfirmPassword;

            if (!checkIfPasswordIsEquals)
                throw new ServiceException("As senhas não combinam.");

            var checkIfEmailExists = _userRepository.GetAllWithInclude(new List<string> { nameof(User.Email) })
                .Where(x => x.Email.Description == user.Email);

            if (checkIfEmailExists != null && checkIfEmailExists.Any())
                throw new ServiceException("Email em uso.");
        }
    }
}
