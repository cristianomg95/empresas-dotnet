﻿using Br.Com.Ioasys.Business.Exceptions;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Br.Com.Ioasys.Sample.Business.Services
{
    public class ConfirmEmailService : IConfirmEmailService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _uow;
        public ConfirmEmailService(IUserRepository userRepository,
            IUnitOfWork uow)
        {
            _userRepository = userRepository;
            _uow = uow;
        }
        public void Execute(DtoEmailConfirm dto)
        {
            var user = _userRepository.GetAllWithInclude(new List<string> { "Email" })
                .FirstOrDefault(x => x.Email.Description == dto.Email && x.Status == StatusType.PreRegistration);

            if (user != null)
            {
                if (user.Email.Code == dto.Code)
                {
                    user.Email.Confirmed = true;
                    user.Status = StatusType.Active;
                    _userRepository.Update(user);
                    _uow.Save();
                    return;
                }
                throw new ServiceException("Codigo invalido.");
            }

            throw new ServiceException("Email não encontrado.");
        }
    }
}
