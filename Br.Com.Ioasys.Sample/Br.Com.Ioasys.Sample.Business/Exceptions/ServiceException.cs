﻿using System;

namespace Br.Com.Ioasys.Business.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException(string msg) : base(msg)
        {

        }
    }
}
