﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Security;
using System.Security.Cryptography;
using System.Text;

namespace Br.Com.Ioasys.Sample.Util.Security
{
    public class Sha256Encryption : IEncryption
    {
        private readonly HashAlgorithm _algoritmo;
        public Sha256Encryption()
        {
            _algoritmo = SHA256.Create();
        }
        public string GenerateCryptgraphy(string password)
        {
            var encodedValue = Encoding.UTF8.GetBytes(password);
            var EncryptedPassword = _algoritmo.ComputeHash(encodedValue);

            var sb = new StringBuilder();
            foreach (var caracter in EncryptedPassword)
            {
                sb.Append(caracter.ToString("X2"));
            }
            return sb.ToString();
        }

        public bool CheckPassword(string password, string encryptedPassword)
        {
            var encryptedCheckedPassword = _algoritmo.ComputeHash(Encoding.UTF8.GetBytes(password));

            var sb = new StringBuilder();
            foreach (var caractere in encryptedCheckedPassword)
            {
                sb.Append(caractere.ToString("X2"));
            }

            return sb.ToString() == encryptedPassword;
        }
    }
}
