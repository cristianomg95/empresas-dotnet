﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ActorController : ControllerBase
    {
        private readonly IActorRepository _actorRepository;
        private readonly IUnitOfWork _uow;
        public ActorController(IActorRepository actorRepository,
            IUnitOfWork uow)
        {
            _actorRepository = actorRepository;
            _uow = uow;
        }


        [Authorize(Roles = nameof(PermissionType.Administration))]
        [HttpPost(Name = "Create-Actor" )]
        public IActionResult CreateActor(DtoCreateActor dto)
        {
            var hasActor = _actorRepository.GetAll().FirstOrDefault(x => x.Name == dto.Name);

            if (hasActor != null)
                return BadRequest("Ator já existe.");

            var newActor = _actorRepository.Insert(new Actor { Name = dto.Name, Description = dto.Description });

            _uow.Save();
            return Ok(newActor);

        }
        [Authorize(Roles = nameof(PermissionType.Administration))]
        [HttpGet(Name = "List-Actores")]
        public IActionResult GetAll()
        {
            return Ok(_actorRepository.GetAll());

        }
    }
}
