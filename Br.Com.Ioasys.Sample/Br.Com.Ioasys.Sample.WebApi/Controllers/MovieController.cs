﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Br.Com.Ioasys.Sample.WebApi.Extensions;
using Br.Com.Ioasys.Sample.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MovieController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IDirectorRepository _directorRepository;
        private readonly IVoteRepository _voteRepository;
        private readonly IWriterRepository _writerRepository;
        private readonly IUnitOfWork _uow;
        public MovieController(IMovieRepository movieRepository,
            IActorRepository actorRepository,
            IDirectorRepository directorRepository,
            IWriterRepository writerRepository,
            IUserRepository userRepository,
            IVoteRepository voteRepository,
            IUnitOfWork uow)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _directorRepository = directorRepository;
            _userRepository = userRepository;
            _voteRepository = voteRepository;
            _writerRepository = writerRepository;
            _uow = uow;
        }

        [HttpGet(Name = "Obter-Filmes")]
        public IActionResult GetAll([FromQuery] MovieFilter filter)
        {
            var movies = _movieRepository
                .GetAllWithInclude(new List<string> 
                {
                    nameof(Movie.Director),
                    nameof(Movie.Actores),
                });

            if (!string.IsNullOrEmpty(filter.Title))
               movies = movies.Where(x => x.Title == filter.Title);

            if (!string.IsNullOrEmpty(filter.Director))
                movies = movies.Where(x => x.Director.Name == filter.Director);

            if (!string.IsNullOrEmpty(filter.Actores))
            {
                var actores = filter.Actores.Split(',').Select(x=>x.Trim());
                IEnumerable<Movie> movieAux = new List<Movie>();
                foreach(var actor in actores)
                {
                    var resultActor = _actorRepository.GetAll().FirstOrDefault(x => x.Name.ToLower() == actor.ToLower());
                    if (resultActor != null)
                    {
                        movieAux = movieAux
                            .Union(movies
                                    .Where(x => x.Actores
                                        .Select(x => x.ActorId)
                                            .Contains(resultActor.Id)));
                    }
                }

                movies = movieAux.AsQueryable();
            }
            movies = movies.OrderBy(x => x.CalculeAverageVote())
                .ThenBy(x=>x.Title);

            if (filter.ApplyPagination)
                movies = movies
                    .Pagination(filter.Page, filter.QuantityPerPage);

            return Ok(movies);
        }
        [HttpGet("{id}", Name = "Obter-Por-Id")]
        public IActionResult GetById(Guid id)
        {
            var movie = _movieRepository.GetById(id);

            if (movie == null)
                return NotFound();

            else
            {
                var result = new DtoResultMovieDetail
                {
                    ReleaseDate = movie.MovieDetail.ReleaseDate,
                    AlsoKnownAs = movie.MovieDetail.AlsoKnownAs,
                    CountryId = movie.MovieDetail.CountryId,
                    Language = movie.MovieDetail.Language,
                    Median = movie.CalculeAverageVote()
                };
                return Ok(result);
            }
        }
        [HttpPut("vote/{id}/{note}", Name = "Votar-no-filme")]
        [Authorize(Roles = nameof(PermissionType.Common))]
        public IActionResult Vote(Guid id, VoteNoteType note)
        {
            var email = User.Claims.FirstOrDefault(x => x.Type.Contains("email")).Value;

            if (email == null)
                return BadRequest("Usuário não encontrado.");

            var user = _userRepository.GetByEmail(email);

            var movie = _movieRepository.GetById(id);

            if (movie == null)
                return BadRequest("Filme não encontrado");

            var vote = movie.Votes.FirstOrDefault(x => x.UserId == user.Id);

            if (vote != null)
            {
                vote.Note = note;
                _voteRepository.Update(vote);
            }
            else
            {
                vote = new Vote { MovieId = movie.Id, UserId = user.Id, Note = note};
                _voteRepository.Insert(vote);
            }
            if (_uow.Save() > 0)
                return Ok(vote);

            return StatusCode((int)HttpStatusCode.NotModified);
        }
        [HttpPost(Name = "Create-Movie")]
        public IActionResult CreateMovie(DtoCreateMovie dto)
        {
            var hasMovieTitle = _movieRepository.GetAll().FirstOrDefault(x => x.Title == dto.Title);

            if (hasMovieTitle != null)
                return BadRequest("O titulo já exite.");

            var actores = _actorRepository.GetAll().Where(x=> dto.Actores.Contains(x.Id)).ToList();

            if (!actores.Any())
                return BadRequest("Um ou mais atores não foi encontrado.");

            var director = _directorRepository.GetAll().FirstOrDefault(x => x.Id == dto.DirectorId);

            if (director == null)
                return BadRequest("Diretor não encontrado.");

            var writers = _writerRepository.GetAll().Where(x => dto.Writes.Contains(x.Id)).ToList();

            if (!writers.Any())
                return BadRequest("Um ou mais escritores não foi encontrado.");

            var movieActor = actores.Select(x => new MovieActor { ActorId = x.Id }).ToList();
            var movieWriter = writers.Select(x => new MovieWrite { WriterId = x.Id }).ToList();

            var newMovie = new Movie
            {
                Title = dto.Title,
                Actores = movieActor,
                Writes = movieWriter,
                Genre = dto.Genre,
                DirectorId = dto.DirectorId,
                Description = dto.Description,
                MovieDetail = new MovieDetail
                {
                    AlsoKnownAs = dto.MovieDetail.AlsoKnownAs,
                    Language = dto.MovieDetail.Language,
                    ReleaseDate = dto.MovieDetail.ReleaseDate,
                    CountryId = dto.MovieDetail.CountryId
                }
            };

            var result = _movieRepository.Insert(newMovie);
            _uow.Save();

            return Ok(result);
        }
    }
}
