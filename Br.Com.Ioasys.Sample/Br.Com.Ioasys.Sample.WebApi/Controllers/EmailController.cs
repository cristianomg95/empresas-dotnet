﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Br.Com.Ioasys.Business.Exceptions;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmailController : ControllerBase
    {
        private readonly IConfirmEmailService _confirmEmailService;
        public EmailController(IConfirmEmailService confirmEmailService)
        {
            _confirmEmailService = confirmEmailService;
        }
        [HttpPut(Name ="Confirmar-email")]
        [AllowAnonymous]
        public IActionResult ConfirmEmail(DtoEmailConfirm dto)
        {
            try
            {
                _confirmEmailService.Execute(dto);
                return Ok();
            }
            catch(ServiceException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
