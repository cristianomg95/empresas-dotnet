﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthenticationService _authService;
        public AuthController(IAuthenticationService authService)
        {
            _authService = authService;
        }
        [HttpPost(Name = "Obter-Autenticação")]
        public IActionResult GetAuth([FromBody] DtoUserCredentials dto)
        {
            var resultAuthentication = _authService.Execute(dto);
            if (resultAuthentication != null)
            {
                return Ok(resultAuthentication);
            }
            return BadRequest("Email ou senha invalido tente novamente.");
        }
    }
}
