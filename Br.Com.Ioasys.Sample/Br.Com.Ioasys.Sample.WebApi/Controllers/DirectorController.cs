﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DirectorController : ControllerBase
    {
        private readonly IDirectorRepository _directorRepository;
        private readonly IUnitOfWork _uow;
        public DirectorController(IDirectorRepository directorRepository,
            IUnitOfWork uow)
        {
            _directorRepository = directorRepository;
            _uow = uow;
        }
        [Authorize(Roles = nameof(PermissionType.Administration))]
        [HttpPost(Name = "Create-Director" )]
        public IActionResult CreateDirector(DtoCreateDirector dto)
        {
            var hasDirector = _directorRepository.GetAll().FirstOrDefault(x => x.Name == dto.Name);

            if (hasDirector != null)
                return BadRequest("Diretor já existe.");

            var newDirector = _directorRepository.Insert(new Director { Name = dto.Name, Description = dto.Description });

            _uow.Save();

            return Ok(newDirector);

        }
        [Authorize(Roles = nameof(PermissionType.Administration))]
        [HttpGet(Name = "List-Directores")]
        public IActionResult GetAll()
        {
            return Ok(_directorRepository.GetAll());

        }
    }
}
