﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Br.Com.Ioasys.Business.Dtos;
using Br.Com.Ioasys.Business.Exceptions;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Br.Com.Ioasys.Sample.WebApi.Extensions;
using Br.Com.Ioasys.Sample.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly ICreateUserService _createUserService;
        private readonly IUpdateUserProfileService _updateUserProfileService;
        private readonly IUnitOfWork _uow;
        public UserController(IUserRepository userRepository,
            ICreateUserService createUserService,
            IUpdateUserProfileService updateUserProfileService,
            IUnitOfWork uow)
        {
            _userRepository = userRepository;
            _createUserService = createUserService;
            _updateUserProfileService = updateUserProfileService;
            _uow = uow;
        }
        [HttpGet(Name = "ObterUsuários")]
        [Authorize(Roles = nameof(PermissionType.Administration))]
        public IActionResult GetAllUsers([FromQuery] Pagination pagination)
        {
            var users = _userRepository.GetAll()
                .Where(x => x.Permission.Name == nameof(PermissionType.Common))
                .OrderBy(x => x.Name);

            if (pagination.ApplyPagination)
                users.Pagination(pagination.Page, pagination.QuantityPerPage);

            return Ok(users);
        }

        [HttpPost(Name = "Criar-Usuário")]
        [AllowAnonymous]
        public IActionResult CreateUser(DtoCreateUser model)
        {
            try
            {
                var newUser = _createUserService.Execute(model, PermissionType.Common);
                return Ok(newUser);
            }
            catch(ServiceException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut(Name = "Alterar-Profile")]
        public IActionResult UpdateUserProfile(DtoUpdateProfile dto)
        {
            var email = User.Claims.FirstOrDefault(x => x.Type.Contains("email")).Value;
            if (email == null)
                return BadRequest("Usuário não encontrado.");

            var updatedUser = _updateUserProfileService.Execute(dto, email);

            if (updatedUser != null)
                return Ok(updatedUser);

            return StatusCode((int)HttpStatusCode.NotModified);
        }

        [HttpDelete]
        public IActionResult DeleteUser()
        {
            var email = User.Claims.FirstOrDefault(x => x.Type.Contains("email")).Value;

            if (email == null)
                return BadRequest("Usuário não encontrado.");

            var user = _userRepository.GetByEmail(email);

            _userRepository.Delete(user);
            _uow.Save();

            return Ok("Usuário deletado.");
        }


    }
}
