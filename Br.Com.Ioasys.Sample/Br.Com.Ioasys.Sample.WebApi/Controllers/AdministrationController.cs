﻿using Br.Com.Ioasys.Business.Exceptions;
using Br.Com.Ioasys.Sample.Domain.Configurations;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AdministrationController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly ICreateUserService _createUserService;
        private readonly Credentials _credentials;
        private readonly IUpdateUserProfileService _updateUserProfileService;
        private readonly IUnitOfWork _uow;


        public AdministrationController(IUserRepository userRepository,
            ICreateUserService createUserService,
            IUpdateUserProfileService updateUserProfileService,
            IUnitOfWork uow,
            Credentials credentials)
        {
            _userRepository = userRepository;
            _credentials = credentials;
            _createUserService = createUserService;
            _updateUserProfileService = updateUserProfileService;
            _uow = uow;

        }
        [AllowAnonymous]
        [HttpPost(Name = "Criar-Usuário-Admnistrador")]
        public IActionResult CreateAdmnistrationUser(DtoCreateAdministrationUser model)
        {
            try
            {
                if (model.SecretKey == _credentials.AdministrationSecretKey)
                {
                    var newUser = _createUserService.Execute(model, PermissionType.Administration);
                    return Ok(newUser);
                }
                return BadRequest("A secret key está invalida.");
            }
            catch (ServiceException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut(Name = "Update-Admnistration-Profile")]
        public IActionResult UpdateAdminitrationProfile(DtoUpdateProfile dto)
        {
            var email = User.Claims.FirstOrDefault(x => x.Type.Contains("email")).Value;

            if (email == null)
                return BadRequest("Usuário não encontrado.");

            var updatedUser = _updateUserProfileService.Execute(dto, email);

            if (updatedUser != null)
                return Ok(updatedUser);

            return StatusCode((int)HttpStatusCode.NotModified);
        }

        [HttpDelete]
        public IActionResult DeleteAdmnistration()
        {
            var email = User.Claims.FirstOrDefault(x => x.Type.Contains("email")).Value;

            if (email == null)
                return BadRequest("Usuário não encontrado.");

            var user = _userRepository.GetByEmail(email);

            _userRepository.Delete(user);
            _uow.Save();

            return Ok("Admnistrador deletado.");
        }
    }
}
