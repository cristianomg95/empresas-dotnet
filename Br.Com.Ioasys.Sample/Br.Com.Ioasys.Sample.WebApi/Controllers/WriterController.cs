﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Domain.Dtos;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Br.Com.Ioasys.Sample.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class WriterController : ControllerBase
    {
        private readonly IWriterRepository _writerRepository;
        private readonly IUnitOfWork _uow;
        public WriterController(IWriterRepository writerRepository,
            IUnitOfWork uow)
        {
            _writerRepository = writerRepository;
            _uow = uow;
        }
        [Authorize(Roles = nameof(PermissionType.Administration))]
        [HttpPost(Name = "Create-Writer" )]
        public IActionResult CreateWriter(DtoCreateWriter dto)
        {
            var hasWriter = _writerRepository.GetAll().FirstOrDefault(x => x.Name == dto.Name);

            if (hasWriter != null)
                return BadRequest("Writer já existe.");

            var newWriter = _writerRepository.Insert(new Writer { Name = dto.Name, Description = dto.Description });
            _uow.Save();

            return Ok(newWriter);

        }
        [Authorize(Roles = nameof(PermissionType.Administration))]
        [HttpGet(Name = "List-Writers")]
        public IActionResult GetAll()
        {
            return Ok(_writerRepository.GetAll());

        }
    }
}
