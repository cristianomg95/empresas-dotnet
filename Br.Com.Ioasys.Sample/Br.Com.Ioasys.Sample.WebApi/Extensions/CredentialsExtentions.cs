﻿using Br.Com.Ioasys.Sample.Domain.Configurations;
using Microsoft.Extensions.DependencyInjection;

namespace Br.Com.Ioasys.Sample.WebApi.Extensions
{
    public static class CredentialsExtentions
    {
        public static void AddCredentials(this IServiceCollection services, string jwtSecret, string administrationSecret)
        {
            services.AddSingleton<Credentials, Credentials>(op =>
            {
                var obj = new Credentials
                {
                    JWTSecretKey = jwtSecret,
                    AdministrationSecretKey = administrationSecret
                };

                return obj;
            });
        }
    }
}
