﻿using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Infra.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;

namespace Br.Com.Ioasys.Sample.WebApi.Extensions
{
    public static class UnitOfWorkExtensions
    {
        public static void AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

        }
    }
}
