﻿using System.Linq;

namespace Br.Com.Ioasys.Sample.WebApi.Extensions
{
    public static class PaginationExtensions
    {
        public static IQueryable<T> Pagination<T>(this IQueryable<T> data, int page, int quantityPerPage)
        {
            return data.Skip(page * quantityPerPage).Take(quantityPerPage);
        }
    }
}
