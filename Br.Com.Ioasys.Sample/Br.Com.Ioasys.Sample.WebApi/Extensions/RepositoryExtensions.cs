﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Infra.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Br.Com.Ioasys.Sample.WebApi.Extensions
{
    public static class RepositoryExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IEmailHistoryRepository, EmailHistoryRepository>();
            services.AddTransient<IPermissionRepository, PermissionRepository>();
            services.AddTransient<IDirectorRepository, DirectorRepository>();
            services.AddTransient<IWriterRepository, WriterRepository>();
            services.AddTransient<IActorRepository, ActorRepository>();
            services.AddTransient<IMovieRepository, MovieRepository>();
            services.AddTransient<IVoteRepository, VoteRepository>();

        }
    }
}

