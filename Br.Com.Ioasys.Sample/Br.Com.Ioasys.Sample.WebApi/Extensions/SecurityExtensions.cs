﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Security;
using Br.Com.Ioasys.Sample.Util.Security;
using Microsoft.Extensions.DependencyInjection;

namespace Br.Com.Ioasys.Sample.WebApi.Extensions
{
    public static class SecurityExtensions 
    {
        public static void AddSecurity(this IServiceCollection services)
        {
            services.AddSingleton<IEncryption, Sha256Encryption>();
        }
    }
}
