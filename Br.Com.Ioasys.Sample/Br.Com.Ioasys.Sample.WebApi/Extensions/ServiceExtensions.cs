﻿using Br.Com.Ioasys.Business.Sample.Services;
using Br.Com.Ioasys.Sample.Business.Services;
using Br.Com.Ioasys.Sample.Domain.Contracts.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Br.Com.Ioasys.Sample.WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {

            services.AddScoped<ISendEmailService, SendEmailService>();
            services.AddScoped<ICreateUserService, CreateUserService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IConfirmEmailService, ConfirmEmailService>();
            services.AddScoped<IUpdateUserProfileService, UpdateUserProfileSerivce>();

        }
    }
}
