﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Br.Com.Ioasys.Sample.WebApi.Models
{
    public class MovieFilter : Pagination
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Director{ get; set; }
        public string Actores{ get; set; }
    }
}
