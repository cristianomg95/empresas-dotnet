﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Br.Com.Ioasys.Sample.WebApi.Models
{
    public class Pagination 
    {
        public int Page { get; set; } = 0;
        public int QuantityPerPage { get; set; } = 10;
        public bool ApplyPagination { get; set; } = false;
    }
}
