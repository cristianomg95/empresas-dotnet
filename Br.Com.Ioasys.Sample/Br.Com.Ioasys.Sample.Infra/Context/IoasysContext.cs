﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Domain.ValueObjects;
using Br.Com.Ioasys.Sample.Infra.Mapping;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Br.Com.Ioasys.Sample.Infra.Context
{
    public class IoasysContext : DbContext
    {
        public IoasysContext(DbContextOptions<IoasysContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserDetail> UserDetails { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<EmailHistory> EmailHistories { get; set; }
        public DbSet<UserEmail> UserEmails { get; set; }
        public DbSet<Actor> Actores{ get; set; }
        public DbSet<Director> Directores { get; set; }
        public DbSet<Writer> Writers { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieDetail> MovieDetails{ get; set; }
        public DbSet<Vote> Votes{ get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.ApplyConfiguration(new ActorMapping());
            modelBuilder.ApplyConfiguration(new CountryMapping());
            modelBuilder.ApplyConfiguration(new DirectorMapping());
            modelBuilder.ApplyConfiguration(new EmailHistoryMapping());
            modelBuilder.ApplyConfiguration(new MovieActorMapping());
            modelBuilder.ApplyConfiguration(new MovieMapping());
            modelBuilder.ApplyConfiguration(new MovieWriteMapping());
            modelBuilder.ApplyConfiguration(new DirectorMapping());
            modelBuilder.ApplyConfiguration(new PermissionMapping());
            modelBuilder.ApplyConfiguration(new UserDetailMapping());
            modelBuilder.ApplyConfiguration(new UserEmailMapping());
            modelBuilder.ApplyConfiguration(new UserMapping());
            modelBuilder.ApplyConfiguration(new VoteMapping());
            modelBuilder.ApplyConfiguration(new WriterMapping());

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Permission>().HasData(
               new Permission
               {
                   Id = Guid.NewGuid(),
                   Name = nameof(PermissionType.Common),
                   Description ="teste"
               },
               new Permission
               {
                   Id = Guid.NewGuid(),
                   Name = nameof(PermissionType.Administration),
                   Description = "teste"
               });


            modelBuilder.Entity<Country>().HasData(
                new Permission
                {
                    Id = Guid.NewGuid(),
                    Name = "Africa", 
                    Description = ""
                },
                new Permission
                {
                    Id = Guid.NewGuid(),
                    Name = "Canada",
                    Description = ""
                },
                new Permission
                {
                    Id = Guid.NewGuid(),
                    Name = "Brasil",
                    Description = ""
                });
        }
    }
}
