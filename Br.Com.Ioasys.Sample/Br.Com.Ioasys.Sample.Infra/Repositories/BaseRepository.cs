﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Infra.Context;
using Br.Com.LojaQueExplode.Infra.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Br.Com.Ioasys.Sample.Infra.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T>
        where T : Entity
        {
            
            private readonly DbSet<T> _dbSet;
            public BaseRepository(IoasysContext context)
            {
                _dbSet = context.Set<T>();
            }
            public T Update(T obj)
            {
                return _dbSet.Update(obj).Entity;
            }

            public void Delete(T obj)
            {
                _dbSet.Remove(obj);
            }
            public T Insert(T obj)
            {
                return _dbSet.Add(obj).Entity;
            }

            public T GetById(Guid id)
            {
                return _dbSet.Find(id);
            }

            public IQueryable<T> GetAll()
            {
                return _dbSet.AsQueryable();
            }

            public IQueryable<T> GetAllWithInclude(List<string> includes)
            {
                var result = _dbSet.AsQueryable();

                foreach (var include in includes)
                {
                    result = result.Include(include);
                }
                return result;
            }
        }
}
