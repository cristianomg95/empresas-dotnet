﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Br.Com.Ioasys.Sample.Infra.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly IoasysContext _context;
        public UserRepository(IoasysContext context) : base(context)
        {
            _context = context;
        }
        public User GetByEmail(string email)
        {
            var user = _context.Users.Include("Email");

            return user.FirstOrDefault(x => x.Email.Description == email);
        }
    }
}
