﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Infra.Context;

namespace Br.Com.Ioasys.Sample.Infra.Repositories
{
    public class WriterRepository : BaseRepository<Writer>, IWriterRepository
    {
        public WriterRepository(IoasysContext context) : base(context)
        {

        }
    }
}
