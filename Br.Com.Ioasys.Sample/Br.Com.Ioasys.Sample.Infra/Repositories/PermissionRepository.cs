﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Infra.Context;
using System.Linq;

namespace Br.Com.Ioasys.Sample.Infra.Repositories
{
    public class PermissionRepository : BaseRepository<Permission>, IPermissionRepository
    {
        private readonly IoasysContext _context;
        public PermissionRepository(IoasysContext context) : base(context)
        {
            _context = context;
        }

        public Permission GetByName(string name)
        {
            return _context.Permissions.FirstOrDefault(x => x.Name == name);
        }
    }
}
