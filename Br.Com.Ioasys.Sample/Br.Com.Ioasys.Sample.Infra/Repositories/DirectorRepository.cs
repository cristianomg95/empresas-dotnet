﻿using Br.Com.Ioasys.Sample.Domain.Contracts.Repositories;
using Br.Com.Ioasys.Sample.Domain.Entities;
using Br.Com.Ioasys.Sample.Infra.Context;

namespace Br.Com.Ioasys.Sample.Infra.Repositories
{
    public class DirectorRepository : BaseRepository<Director>, IDirectorRepository
    {
        public DirectorRepository(IoasysContext context ) : base(context)
        {
        }
    }
}
