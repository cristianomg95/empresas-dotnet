﻿using Br.Com.Ioasys.Sample.Domain.Contracts.UnitOfWork;
using Br.Com.Ioasys.Sample.Infra.Context;

namespace Br.Com.Ioasys.Sample.Infra.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IoasysContext _context;
        public UnitOfWork(IoasysContext context)
        {
            _context = context;
        }
        public int Save()
        {
            return _context.SaveChanges();
        }


    }
}
