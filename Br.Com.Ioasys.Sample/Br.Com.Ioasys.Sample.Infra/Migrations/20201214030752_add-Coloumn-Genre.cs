﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Br.Com.Ioasys.Sample.Infra.Migrations
{
    public partial class addColoumnGenre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("24aa4caf-b743-4084-9d85-058c7abe4aae"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("55dff1bb-17e9-44e6-bc37-8659162bba11"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("6a6c30f1-4b8c-46b2-9f47-571bdc072605"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("2b2a6e69-c9f7-4e54-8c48-ae3351d10ed0"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("f54946b9-04aa-4d30-8d9d-ce0427b0c384"));

            migrationBuilder.AddColumn<string>(
                name: "Genre",
                table: "Movies",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("b6a84df0-4991-4253-8bb9-4050a455a611"), "Africa" },
                    { new Guid("d97a8780-7d9f-48cf-9894-42876c30035a"), "Canada" },
                    { new Guid("bfaad56c-5cad-4faf-ac56-85dc74d7807d"), "Brasil" }
                });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { new Guid("e4d1a1ab-7225-4abc-bef0-f6e5d7fdba27"), "teste", "Common" },
                    { new Guid("0e9af9d3-5a04-42ff-92b2-4d8c4f6a2580"), "teste", "Administration" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("b6a84df0-4991-4253-8bb9-4050a455a611"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("bfaad56c-5cad-4faf-ac56-85dc74d7807d"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("d97a8780-7d9f-48cf-9894-42876c30035a"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("0e9af9d3-5a04-42ff-92b2-4d8c4f6a2580"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("e4d1a1ab-7225-4abc-bef0-f6e5d7fdba27"));

            migrationBuilder.DropColumn(
                name: "Genre",
                table: "Movies");

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("6a6c30f1-4b8c-46b2-9f47-571bdc072605"), "Africa" },
                    { new Guid("24aa4caf-b743-4084-9d85-058c7abe4aae"), "Canada" },
                    { new Guid("55dff1bb-17e9-44e6-bc37-8659162bba11"), "Brasil" }
                });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { new Guid("f54946b9-04aa-4d30-8d9d-ce0427b0c384"), "teste", "Common" },
                    { new Guid("2b2a6e69-c9f7-4e54-8c48-ae3351d10ed0"), "teste", "Administration" }
                });
        }
    }
}
