﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Br.Com.Ioasys.Sample.Infra.Migrations
{
    public partial class addcolumncodeonemailentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1f57ebe6-602e-45d0-8e76-275f0efa06c1"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("7a83cf17-df2a-4661-a244-62d6ba7b275a"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("83fb3b5c-fe87-40fe-a80e-fb1f982e13bc"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("2dd16571-8afd-464d-84fc-a6088e793885"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("9e48fd64-7165-43ec-a2ff-e7dc4cc44f1e"));

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "UserEmails",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("da059c54-8f79-4ba3-b8ac-c1efd12c390a"), "Africa" },
                    { new Guid("83f0918a-7574-4bda-a76a-9a6a328bcd93"), "Canada" },
                    { new Guid("f338f1ca-3d81-4379-b8ac-3320d5e3a5d7"), "Brasil" }
                });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { new Guid("41df9308-1d1d-4058-9bf7-69972f3fc0c3"), "teste", "Common" },
                    { new Guid("84943ae4-bf57-4ef5-9370-8c769b760788"), "teste", "Administration" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("83f0918a-7574-4bda-a76a-9a6a328bcd93"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("da059c54-8f79-4ba3-b8ac-c1efd12c390a"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("f338f1ca-3d81-4379-b8ac-3320d5e3a5d7"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("41df9308-1d1d-4058-9bf7-69972f3fc0c3"));

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: new Guid("84943ae4-bf57-4ef5-9370-8c769b760788"));

            migrationBuilder.DropColumn(
                name: "Code",
                table: "UserEmails");

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("83fb3b5c-fe87-40fe-a80e-fb1f982e13bc"), "Africa" },
                    { new Guid("1f57ebe6-602e-45d0-8e76-275f0efa06c1"), "Canada" },
                    { new Guid("7a83cf17-df2a-4661-a244-62d6ba7b275a"), "Brasil" }
                });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { new Guid("2dd16571-8afd-464d-84fc-a6088e793885"), "teste", "Common" },
                    { new Guid("9e48fd64-7165-43ec-a2ff-e7dc4cc44f1e"), "teste", "Administration" }
                });
        }
    }
}
