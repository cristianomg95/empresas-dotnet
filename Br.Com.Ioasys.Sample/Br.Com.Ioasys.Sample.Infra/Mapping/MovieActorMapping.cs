﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class MovieActorMapping : IEntityTypeConfiguration<MovieActor>
    {
        public void Configure(EntityTypeBuilder<MovieActor> builder)
        {
            builder.HasKey(x => new { x.MovieId, x.ActorId });

            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Actores)
                .HasForeignKey(x => x.MovieId);

            builder.HasOne(x => x.Actor)
                .WithMany(x => x.Movies)
                .HasForeignKey(x => x.ActorId);
        }
    }
}
