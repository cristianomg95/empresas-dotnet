﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(255);

            builder.Property(x => x.Password).IsRequired();

            builder.Property(x => x.NickName).IsRequired().HasMaxLength(255);

            builder.HasIndex(x => x.NickName).IsUnique();

            builder.Property(x => x.Bio).HasMaxLength(255);

            builder.HasOne(user => user.Permission)
                .WithMany(permission => permission.Users)
                .HasForeignKey(user => user.PermissionId);

            builder.HasOne(user => user.UserDetail)
                .WithOne(userDetail => userDetail.User)
                .HasForeignKey<UserDetail>(user => user.UserId);

            builder.HasOne(x => x.Email)
                .WithOne(x => x.User)
                .HasForeignKey<UserEmail>(x => x.UserId);

        }
    }
}
