﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class VoteMapping : IEntityTypeConfiguration<Vote>
    {
        public void Configure(EntityTypeBuilder<Vote> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Votes)
                .HasForeignKey(x => x.MovieId);

            builder.HasOne(x => x.User)
               .WithMany(x => x.Votes)
               .HasForeignKey(x => x.UserId);
        }
    }
}
