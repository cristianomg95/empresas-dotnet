﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class MovieWriteMapping : IEntityTypeConfiguration<MovieWrite>
    {
        public void Configure(EntityTypeBuilder<MovieWrite> builder)
        {
            builder.HasKey(x => new { x.MovieId, x.WriterId });

            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Writes)
                .HasForeignKey(x => x.MovieId);


            builder.HasOne(x => x.Writer)
                .WithMany(x => x.Movies)
                .HasForeignKey(x => x.WriterId);
        }
    }
}
