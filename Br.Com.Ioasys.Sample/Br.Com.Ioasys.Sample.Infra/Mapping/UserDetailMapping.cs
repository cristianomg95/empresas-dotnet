﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class UserDetailMapping : IEntityTypeConfiguration<UserDetail>
    {
        public void Configure(EntityTypeBuilder<UserDetail> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Gender).IsRequired().HasMaxLength(1);
            builder.Property(x => x.DateOfBirthday).IsRequired();
            builder.Property(x => x.CountryId).IsRequired();

            builder.HasOne(userDetail => userDetail.Country)
                .WithMany(country => country.UserDetails)
                .HasForeignKey(userDetail => userDetail.CountryId);

            builder.HasOne(userDetail => userDetail.User)
                .WithOne(user => user.UserDetail)
                .HasForeignKey<UserDetail>(user => user.UserId);
        }
    }
}
