﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class MovieMapping : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).IsRequired().HasMaxLength(255);
            builder.Property(x => x.Description).IsRequired();

            builder.HasIndex(x => x.Title).IsUnique();

            builder.HasOne(x => x.Director)
                .WithMany(x => x.Movies)
                .HasForeignKey(x => x.DirectorId);

            builder.HasMany(x => x.Writes)
                .WithOne(x => x.Movie)
                .HasForeignKey(x => x.MovieId);

            builder.HasMany(x => x.Actores)
                .WithOne(x => x.Movie)
                .HasForeignKey(x => x.MovieId);

            builder.HasMany(x => x.Votes)
                .WithOne(x => x.Movie)
                .HasForeignKey(x => x.MovieId);

            builder.HasOne(x => x.MovieDetail)
                .WithOne(x => x.Movie)
                .HasForeignKey<MovieDetail>(x=>x.MovieId);

        }
    }
}
