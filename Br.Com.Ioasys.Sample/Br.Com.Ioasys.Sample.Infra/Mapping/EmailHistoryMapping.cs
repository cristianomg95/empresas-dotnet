﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class EmailHistoryMapping : IEntityTypeConfiguration<EmailHistory>
    {
        public void Configure(EntityTypeBuilder<EmailHistory> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.EmailTo).IsRequired();
            builder.Property(x => x.Subject).IsRequired();
            builder.Property(x => x.Content).IsRequired();
            builder.Property(x => x.Sended).IsRequired().HasDefaultValue(false);
        }
    }
}
