﻿using Br.Com.Ioasys.Sample.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Br.Com.Ioasys.Sample.Infra.Mapping
{
    public class UserEmailMapping : IEntityTypeConfiguration<UserEmail>
    {
        public void Configure(EntityTypeBuilder<UserEmail> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Description).IsRequired();
            builder.Property(x => x.Confirmed).HasDefaultValue(false);
            builder.Property(x => x.Code).IsRequired();

            builder.HasIndex(x => x.Description).IsUnique();

            builder.HasOne(x => x.User)
                .WithOne(x => x.Email)
                .HasForeignKey<UserEmail>(x => x.UserId);
        }
    }
}
